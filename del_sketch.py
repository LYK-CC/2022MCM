import matplotlib.pyplot as plt
from numpy import loadtxt
import numpy as np

x = loadtxt('original_gold.txt')
print(len(x))
y = [i for i in range(0,1823)]
x2 = np.polyfit(x,y,1)
x3 = np.poly1d(x2)
plt.figure() 
plt.plot(x,x3)
plt.show()
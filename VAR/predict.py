# 模型阶数从1开始逐一增加
rows, cols = subdata_diff1.shape
aicList = []
lmList = []

for p in range(1,11):
   baseData = None
    for i in range(p,rows):
        tmp_list = list(subdata_diff1[i,:]) + list(subdata_diff1[i-p:i].flatten())
        if baseData is None:
            baseData = [tmp_list]
        else:
            baseData = np.r_[baseData, [tmp_list]]
    X = np.c_[[1]*baseData.shape[0],baseData[:,cols:]]
    Y = baseData[:,0:cols]
    coefMatrix = np.matmul(np.matmul(np.linalg.inv(np.matmul(X.T,X)),X.T),Y)
    aic = np.log(np.linalg.det(np.cov(Y - np.matmul(X,coefMatrix),rowvar=False))) + 2*(coefMatrix.shape[0]-1)**2*p/baseData.shape[0]
    aicList.append(aic)
    lmList.append(coefMatrix)

#对比查看阶数和AIC
pd.DataFrame({"P":range(1,11),"AIC":aicList})
#   P   AIC
# 0 1   13.580156
# 1 2   13.312225
# 2 3   13.543633
# 3 4   14.266087
# 4 5   15.512437
# 5 6   17.539047
# 6 7   20.457337
# 7 8   24.385459
# 8 9   29.438091
# 9 10  35.785909
p = np.argmin(aicList)+1
n = rows
preddf = None
for i in range(30):
    predData = list(subdata_diff1[n+i-p:n+i].flatten())
    predVals = np.matmul([1]+predData,lmList[p-1])
    # 使用逆差分运算，还原预测值
    predVals=data.iloc[n+i,:].values[:4]+predVals
    if preddf is None:
         preddf = [predVals]
    else:
        preddf = np.r_[preddf, [predVals]]
    # 为subdata_diff1增加一条新记录
    subdata_diff1 = np.r_[subdata_diff1, [data.iloc[n+i+1,:].values[:4] - data.iloc[n+i,:].values[:4]]]

#分析预测残差情况
(np.abs(preddf - data.iloc[-30:data.shape[0],:4])/data.iloc[-30:data.shape[0],:4]).describe()
#       High         Low         Open       Close
# count 30.000000   30.000000   30.000000   30.000000
# mean  0.010060    0.009380    0.005661    0.013739
# std   0.008562    0.009968    0.006515    0.013674
# min   0.001458    0.000115    0.000114    0.000130
# 25%   0.004146    0.001950    0.001653    0.002785
# 50%   0.007166    0.007118    0.002913    0.010414
# 75%   0.014652    0.012999    0.006933    0.022305
# max   0.039191    0.045802    0.024576    0.052800
 
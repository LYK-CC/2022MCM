import matplotlib.pyplot as plt
plt.figure(figsize=(10,7))
for i in range(4):
    plt.subplot(2,2,i+1)
    plt.plot(range(30),data.iloc[-30:data.shape[0],i].values,'o-',c='black')
    plt.plot(range(30),preddf[:,i],'o--',c='gray')
    plt.ylim(1000,1200)
    plt.ylabel("$"+data.columns[i]+"$")
plt.show()
v = 100*(1 - np.sum(np.abs(preddf - data.iloc[-30:data.shape[0],:4]).values)/np.sum(data.iloc[-30:data.shape[0],:4].values))
print("Evaluation on test data: accuracy = %0.2f%% \n" % v)
# Evaluation on test data: accuracy = 99.03%
 
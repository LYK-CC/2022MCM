import statsmodels.tsa.stattools as stat
import pandas_datareader.data as web
import datetime as dt
import pandas as pd
import numpy as np
 
data = web.DataReader('600519.ss','yahoo', dt.datetime(2014,1,1),dt.datetime(2019,9,30))
subdata = data.iloc[:-30,:4]
for i in range(4):
    pvalue = stat.adfuller(subdata.values[:,i], 1)[1]
    print("指标 ",data.columns[i]," 单位根检验的p值为：",pvalue)
# 指标  High  单位根检验的p值为：0.9955202280850401
# 指标  Low  单位根检验的p值为：0.9942509439755689
# 指标  Open  单位根检验的p值为：0.9938548193990323
# 指标  Close  单位根检验的p值为：0.9950049124079876

subdata_diff1 = subdata.iloc[1:,:].values - subdata.iloc[:-1,:].values
for i in range(5):
    pvalue = stat.adfuller(subdata_diff1[:,i], 1)[1]
    print("指标 ",data.columns[i]," 单位根检验的p值为：",pvalue)
    
# 指标  High  单位根检验的p值为：0.0
# 指标  Low  单位根检验的p值为：0.0
# 指标  Open  单位根检验的p值为：0.0
# 指标  Close  单位根检验的p值为：0.0
 
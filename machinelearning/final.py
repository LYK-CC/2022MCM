import math
from re import M
from pandas import read_csv
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM
import matplotlib.pyplot as plt
from sympy import N, Max




scaler = MinMaxScaler(feature_range=(0,1))

trainingprecentage = 0.8
traindatanums = 5  #according to the market condition -open or close
learn_time = 150 # default learning time per each:100
batch_size = 3 #its about programs, no need explanation

data_main = read_csv(r'train.csv', header=0, parse_dates=[0], index_col=0, squeeze=True)



data_values = data_main.values
training_data_lens = int(len(data_values)*trainingprecentage)




training_data_lens = 120 #TODO manual overide





#-------------------------------
data_values2 = data_values
np.set_printoptions(suppress=True)
data_values = data_values.reshape(-1, 1)
data_values = scaler.fit_transform(data_values)
#-------------------------------
train_data = data_values[0:training_data_lens]

x_train = []
y_train = []

for i in range(traindatanums,len(train_data)):
    x_train.append(train_data[i-traindatanums:i])
    y_train.append(train_data[i])

x_train,y_train = np.array(x_train),np.array(y_train)

x_train = np.reshape(x_train, (x_train.shape[0],x_train.shape[1],1))


#--------------------buildLSTM
model = Sequential()
model.add(LSTM(50, return_sequences=True, input_shape = (x_train.shape[1],1), ))
model.add(LSTM(50,return_sequences=False))
model.add(Dense(25))
model.add(Dense(1))

model.compile(optimizer = 'adam', loss = 'mean_squared_error')
model.fit(x_train,y_train,batch_size = batch_size, epochs = learn_time)


#_______________________________test // single calcu

test_data = data_values[training_data_lens - traindatanums : ]
x_test = []

x_test.append(test_data[-7:-2])
x_test.append(test_data[-6:-1])
#(1645, 60, 1)
x_test = np.reshape(x_test,(2, 5, 1))#TODO

predictions = model.predict(x_test)
prediction = scaler.inverse_transform(predictions)
print(prediction)
#--------------------------double calcu

#TODO
#training_data_lens = int(len(data_values)*trainingprecentage)

test_data = data_values[training_data_lens - traindatanums : ]
x_test = []

for i in range(traindatanums,len(test_data)):
    x_test.append(test_data[i-traindatanums:i])
x_test = np.array(x_test)

x_test = np.reshape(x_test,(x_test.shape[0],x_test.shape[1],1))

predictions = model.predict(x_test)
prediction = scaler.inverse_transform(predictions)




plt.plot(prediction,color = "blue",label='predicted value')
plt.plot(data_values2[training_data_lens: ], color = "orange",label='true value')
plt.xlabel('days starting from 2016.9.11')
plt.ylabel('price of BTC in $')
plt.legend()
plt.show()

np.savetxt("50-80-gold.txt",prediction,fmt="%.8f")

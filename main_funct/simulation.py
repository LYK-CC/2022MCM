from numbers import Real
import numpy as np
from sympy import proper_divisor_count;
import matplotlib.pyplot as plt

gol_trans_cost = 0.03#TODO
btc_trans_cost = 0.04#TODO
property = 1087.842; #the init property that we have

gol_pre =  np.loadtxt("50-100-GOL.txt",dtype=float);
btc_pre = np.loadtxt("50-100-BTC.txt",dtype=float);

btc_real = np.loadtxt("orginial_BTC.txt",dtype=float);
gol_real = np.loadtxt("original_gold.txt",dtype=float);

data_lens = len(btc_pre)

def P1(cointype,gol_now,gol_pre,btc_now,btc_pre,property_now_in_dollar):
    if cointype == 'GOL':
        return property_now_in_dollar*((gol_pre-gol_now)/gol_now)+((property_now_in_dollar*(1-gol_trans_cost))/btc_pre*(1+btc_trans_cost))*btc_pre-property_now_in_dollar*(1+(gol_pre-gol_now)/gol_now)-(property_now_in_dollar*gol_trans_cost+(property_now_in_dollar*(1-gol_trans_cost))/(btc_pre*(1+btc_trans_cost))*btc_pre*btc_trans_cost);
    if cointype == 'BTC':
        return property_now_in_dollar*((btc_pre-btc_now)/btc_now)+((property_now_in_dollar*(1-btc_trans_cost))/gol_pre*(1+gol_trans_cost))*gol_pre-property_now_in_dollar*(1+(btc_pre-btc_now)/btc_now)-(property_now_in_dollar*btc_trans_cost+(property_now_in_dollar*(1-btc_trans_cost))/(gol_pre*(1+gol_trans_cost))*gol_pre*gol_trans_cost);
def P2(cointype,gol_now,gol_pre,btc_now,btc_pre,property_now_in_dollar):
    if cointype == 'GOL':
        return (((property_now_in_dollar*(1-gol_trans_cost))/btc_pre*(1+btc_trans_cost))*btc_pre-property_now_in_dollar*(1+(gol_pre-gol_now)/gol_now)-(property_now_in_dollar*gol_trans_cost+(property_now_in_dollar*(1-gol_trans_cost))/(btc_pre*(1+btc_trans_cost))*btc_pre*btc_trans_cost));
    if cointype == 'BTC':
        return  (((property_now_in_dollar*(1-btc_trans_cost))/gol_pre*(1+gol_trans_cost))*gol_pre-property_now_in_dollar*(1+(btc_pre-btc_now)/btc_now)-(property_now_in_dollar*btc_trans_cost+(property_now_in_dollar*(1-btc_trans_cost))/(gol_pre*(1+gol_trans_cost))*gol_pre*gol_trans_cost));
        

#day == day need to be predicted, tomorrow
def spec(day,cointype):
    if cointype == 'BTC':
        if btc_pre[day]>btc_pre[day-1]:
            return (btc_pre[day]-btc_pre[day-1])/btc_pre[day];
        elif btc_pre[day]<btc_pre[day-1]:
            return -(btc_pre[day-1]-btc_pre[day])/btc_pre[day-1];
    if cointype == 'GOL':
        if gol_pre[day]>gol_pre[day-1]:
            return (gol_pre[day]-gol_pre[day-1])/gol_pre[day];
        elif gol_pre[day]<=gol_pre[day-1]:
            return -(gol_pre[day-1]-gol_pre[day])/gol_pre[day-1];

def real(day,cointype):
    if cointype == 'BTC':
        if btc_real[day]>btc_real[day-1]:
            return (btc_real[day]-btc_real[day-1])/btc_real[day];
        elif btc_real[day]<=btc_real[day-1]:
            return -((btc_real[day-1]-btc_real[day])/btc_real[day-1]);
    if cointype == 'GOL':
        if gol_real[day]>gol_real[day-1]:
            return (gol_real[day]-gol_real[day-1])/gol_real[day];
        elif gol_real[day]<=gol_real[day-1]:
            return -((gol_real[day-1]-gol_real[day])/gol_real[day-1]);

axisdata = []
for j in range(0,30):
    property = 1087.842;
    gol_trans_cost = j*0.001#TODO
    btc_trans_cost = 0.01+j*0.001#TODO
    hold = 'None'
    count=0
    flag = 'NEGATIVE'
    listt = []
    for i in range(2,913):
        
        btc = real(i,'BTC');
        gol = real(i,'GOL');
        if flag != hold:
            count+=1
        flag = hold
        
        if hold == 'BTC':
            property *= 1+real(i-1,'BTC');
        elif hold == 'GOL':
            property *= 1+real(i-1,'GOL');
        listt.append(property)
        print(property)
        if (hold == 'BTC' and btc > 0 and gol <= 0) or (hold == 'GOL' and gol > 0 and btc <= 0):
            continue
        if btc<=0 and gol<=0:
            if hold == 'BTC':
                property-= property*btc_trans_cost
                hold = 'None';
            if hold == 'GOL':
                property-= property*gol_trans_cost
                hold = 'None';
            if hold == 'None':
                continue;
        
        if hold == 'GOL' and btc > 0 and gol <= 0 :
            if P1('GOL',gol_real[i-1],gol_pre[i],btc_real[i-1],btc_pre[i],property) >= 0:
                property = property - property*gol_trans_cost
                property = property - property*btc_trans_cost
                hold = 'BTC'
            else:
                continue
        elif hold == 'GOL' and btc > 0 and gol > 0:
            if P2('GOL',gol_real[i-1],gol_pre[i],btc_real[i-1],btc_pre[i],property) >= 0:
                property = property - property*gol_trans_cost
                property = property - property*btc_trans_cost
                hold = 'BTC'
            else:
                continue
        if hold == 'BTC' and gol > 0 and btc < 0 :
            if P1('BTC',btc_real[i-1],btc_pre[i],gol_real[i-1],gol_pre[i],property) >= 0:
                property = property - property*btc_trans_cost
                property = property - property*gol_trans_cost
                hold = 'GOL'
            else:
                continue
        elif hold == 'BTC' and gol > 0 and btc > 0:
            if P2('BTC',btc_real[i-1],btc_pre[i],gol_real[i-1],gol_pre[i],property) >= 0:
                property = property - property*btc_trans_cost
                property = property - property*gol_trans_cost
                hold = 'GOL'
            else:
                continue
        if hold == 'None':
            if btc > 0 and gol <= 0:
                property = property - property * btc_trans_cost
                hold = 'BTC'
            if btc < 0 and gol > 0:
                property = property - property * gol_trans_cost
                hold = 'GOL'
            if btc > 0 and gol > 0:
                if btc > gol:
                    property = property - property * btc_trans_cost
                    hold = 'BTC'
                if gol > btc:
                    property = property - property * gol_trans_cost
                    hold = 'GOL'
    axisdata.append(float(listt[-1:][0]))
    print(axisdata)
    
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

ax = plt.gca()


plt.plot(axisdata)

    
        

            



    

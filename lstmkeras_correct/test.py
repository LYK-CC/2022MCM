#-*- coding: utf-8 -*-
from numpy import array
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from pandas import read_csv
from pandas import datetime
from pandas import DataFrame
from pandas import concat
from matplotlib import pyplot


series = read_csv(r'hdx_copy.csv', header=0, parse_dates=[0], index_col=0, squeeze=True)
series_size = len(array(series))

epo = 100

'''
将数据转换成有监督数据
训练的目的就是找到训练数据input和output的关系
具体实现就是将整体的时间数据向后滑动n格，和原始数据拼接，就是有监督的数据
'''
# 把data数组构建成监督学习型数据
# lag：步长
def timeseries_to_supervised(data, lag=1):  
    df = DataFrame(data)
    # 原始数据时间窗向后移动lag步长
    colums = [df.shift(i) for i in range( lag + 1,1,-1)]
    # 拼接数据
    colums.append(df.shift(axis=0, periods=1))  
    # 横向拼接重塑数据
    df = concat(colums, axis=1)  
    print(df)
    # 删除nan数据
    df.dropna(inplace=True)  
    return df

X = series.values
supervised = timeseries_to_supervised(X, 100) #TODO
print(supervised)
print(supervised.values[:,0:-1])
print(supervised.values[:,-1])

# 定义数据
X = supervised.values[:,0:-1]
X = X.reshape(X.shape[0], X.shape[1], 1)
y = supervised.values[:,-1]

# 定义模型
model = Sequential()
model.add(LSTM(50, activation='relu', input_shape=(X.shape[1], X.shape[2])))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mae')
print(model.summary())
# 训练模型
model.fit(X, y, epochs=0, verbose=1)

print('学习完成')
# 开始预测

list = []
for i in range(0,1820):
    x_input = array(read_csv(r'hdx.csv', header=0, parse_dates=[0], index_col=0, squeeze=True).values[i:i+100])
    x_input = x_input.reshape((1, 100, 1))
    yhat = model.predict(x_input, verbose=0)
    print(yhat[0][0])
    list.append(yhat[0][0])
pyplot.plot(list)


    
    
    